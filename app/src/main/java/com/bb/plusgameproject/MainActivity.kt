package com.bb.plusgameproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.bb.plusgameproject.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*

object GlobalVariable {
    var correct = 0
    var incorrect = 0
}

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        generateQuestions()
    }

    private fun generateQuestions() {
        binding.apply {
            val randomNum1 = (0..10).random()
            val randomNum2 = (0..10).random()
            num1.text = randomNum1.toString()
            num2.text = randomNum2.toString()
            val ans = randomNum1 + randomNum2
            generateAnswer(ans)
        }
    }

    private fun generateAnswer(ans: Int) {
        binding.apply {
            val answers = arrayOf(
                "btnAns1", "btnAns2", "btnAns3"
            )
            val btnRandom = answers[(0..2).random()]
            if (btnRandom == "btnAns1") {
                btnAns1.text = ans.toString()
                btnAns2.text = (0..20).random().toString()
                btnAns3.text = (0..20).random().toString()
            } else if (btnRandom == "btnAns2") {
                btnAns1.text = (0..20).random().toString()
                btnAns2.text = ans.toString()
                btnAns3.text = (0..20).random().toString()
            } else {
                btnAns1.text = (0..20).random().toString()
                btnAns2.text = (0..20).random().toString()
                btnAns3.text = ans.toString()
            }
            checkAnswer(ans)
        }
    }

    private fun checkAnswer(ans: Int) {
        checkButton1(ans)
        checkButton2(ans)
        checkButton3(ans)
    }

    private fun checkButton1(ans: Int) {
        btnAns1.setOnClickListener {
            val checkAns = btnAns1.text.toString()
            if (checkAns.toInt() == ans) {
                Toast.makeText( this@MainActivity, "ถูกต้อง", Toast.LENGTH_LONG).show()
                GlobalVariable.correct++
                txtCorrect.text = GlobalVariable.correct.toString()
            } else {
                Toast.makeText(this@MainActivity, "ไม่ถูกต้อง", Toast.LENGTH_LONG).show()
                GlobalVariable.incorrect++
                txtIncorrect.text = GlobalVariable.incorrect.toString()
            }
            generateQuestions()
        }
    }

    private fun checkButton2(ans: Int) {
        btnAns2.setOnClickListener {
            val checkAns = btnAns2.text.toString()
            if (checkAns.toInt() == ans) {
                Toast.makeText(this@MainActivity, "ถูกต้อง", Toast.LENGTH_LONG).show()
                GlobalVariable.correct++
                txtCorrect.text = "ถูก : "+"${GlobalVariable.correct}"
            } else {
                Toast.makeText(this@MainActivity, "ไม่ถูกต้อง", Toast.LENGTH_LONG).show()
                GlobalVariable.incorrect++
                txtIncorrect.text = "ผิด : "+"${GlobalVariable.incorrect}"
            }
            generateQuestions()
        }
    }

    private fun checkButton3(ans: Int) {
        btnAns3.setOnClickListener {
            val checkAns = btnAns3.text.toString()
            if (checkAns.toInt() == ans) {
                Toast.makeText(this@MainActivity, "ถูกต้อง", Toast.LENGTH_LONG).show()
                GlobalVariable.correct++
                txtCorrect.text = "ถูก : "+"${GlobalVariable.correct}"
            } else {
                Toast.makeText(this@MainActivity, "ไม่ถูกต้อง", Toast.LENGTH_LONG).show()
                GlobalVariable.incorrect++
                txtIncorrect.text = "ผิด : "+"${GlobalVariable.incorrect}"
            }
            generateQuestions()
        }
    }
}